/**************************************************************************/ /**
 * @file
 * @brief This file contains the members of the CommandLineInterface class.
 *****************************************************************************/
#include "commandLineInterface.h"

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * The constructor CommandLineInterface
 ******************************************************************************/
CommandLineInterface::CommandLineInterface()
{
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process a gived command
 * 
 * @param[in] commandList - the list of commands
 ******************************************************************************/
void CommandLineInterface::processCommand(std::list<std::string> commandList)
{
    int cpid;
    int stat;
    char *args[25];

    buildCharArrayFromList(commandList, args);

    //start child process
    cpid = fork();
    if (cpid == 0)
    {
        execvp(args[0], args);
        perror("Command execution failed: ");
        exit(5);
    }
    else
    {
        std::cout << "Child process stated. Pid: " << cpid << "\n\n";
    }

    cpid = wait(&stat);

    struct rusage usage;
    getrusage(RUSAGE_CHILDREN, &usage);
    std::cout << "\nStats for child processes";
    std::cout << "\n  User Time:   " << usage.ru_utime.tv_usec << " ms";
    std::cout << "\n  System Time: " << usage.ru_stime.tv_usec << " ms";
    std::cout << "\n  Page Faults: " << usage.ru_majflt;
    std::cout << "\n  Swaps:       " << usage.ru_nswap << "\n\n";
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process a gived command with redirection
 * 
 * @param[in] commandList - the list of commands
 * @return - the result of the command
 ******************************************************************************/
void CommandLineInterface::processRedirection(std::list<std::string> commandList)
{
    std::list<std::string>::iterator stdoutPos, stdinPos;
    std::string stdoutFile = "", stdinFile = "";
    std::list<std::string> initialComand;
    int fd,cpid, stat;
    char *args[25];

    stdoutPos = std::find(commandList.begin(), commandList.end(), ">");
    stdinPos = std::find(commandList.begin(), commandList.end(), "<");

    //get the commands by themselves
    if (std::distance(commandList.begin(), stdoutPos) > std::distance(commandList.begin(), stdinPos))
    {
        initialComand.splice(initialComand.begin(), commandList, commandList.begin(), stdinPos);
    }
    else
    {
        initialComand.splice(initialComand.begin(), commandList, commandList.begin(), stdoutPos);
    }
    buildCharArrayFromList(initialComand, args);

    //create input and output file names
    if (stdoutPos != commandList.end())
    {
        stdoutPos++;
        if (stdoutPos == commandList.end())
        {
            std::cout << "Malformed command\n\n";
            return;
        }
        stdoutFile = *stdoutPos;
    }
    if (stdinPos != commandList.end())
    {
        stdinPos++;
        if (stdinPos == commandList.end())
        {
            std::cout << "Malformed command\n\n";
            return;
        }
        stdinFile = *stdinPos;
    }

    //start child process
    cpid = fork();
    if (cpid == 0)
    {
        //redirect output, if applicable
        if (stdoutFile != "")
        {
            if ((fd = creat(stdoutFile.c_str(), 0644)) == -1)
            {
                std::cout << "Unable to open " << stdoutFile << " for writing.\n";
                exit(-1);
            }

            close(1);
            dup2(fd, 1);
            close(fd);
        }

        //redirect input, if applicable
        if (stdinFile != "")
        {
            if ((fd = open(stdinFile.c_str(), O_RDONLY)) == -1)
            {
                std::cout << "Unable to open " << stdinFile << " for reading.\n";
                exit(-1);
            }

            close(0);
            dup2(fd, 0);
            close(fd);
        }

        //execute command
        execvp(args[0], args);
        perror("Command execution failed: ");
        exit(5);
    }
    else
    {
        std::cout << "Child process stated. Pid: " << cpid << "\n\n";
    }

    cpid = wait(&stat);

    struct rusage usage;
    getrusage(RUSAGE_CHILDREN, &usage);
    std::cout << "\nStats for child processes";
    std::cout << "\n  User Time:   " << usage.ru_utime.tv_usec << " ms";
    std::cout << "\n  System Time: " << usage.ru_stime.tv_usec << " ms";
    std::cout << "\n  Page Faults: " << usage.ru_majflt;
    std::cout << "\n  Swaps:       " << usage.ru_nswap << "\n\n";
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process a gived command with a pipe
 * 
 * @param[in] commandList - the list of commands
 * @return - the result of the command
 ******************************************************************************/
void CommandLineInterface::processPipe(std::list<std::string> commandList)
{
    std::list<std::list<std::string>> pipeSeparatedCommandList;
    char *args1[25], *args2[25];
    int pid1, pid2, stat;
    int fd_pipe[2];

    if (pipeSeparatedCommandList.size() > 2)
    {
        std::cout << "Multiple pipes are not supported\n\n";
    }

    pipeSeparatedCommandList = splitList(commandList, "|");
    buildCharArrayFromList(pipeSeparatedCommandList.back(), args1);
    pipeSeparatedCommandList.pop_back();
    buildCharArrayFromList(pipeSeparatedCommandList.back(), args2);

    pid1 = fork();
    if (pid1 == 0)
    {
        //open the pipe
        if (pipe(fd_pipe) != 0)
        {
            std::cout << "Error: Could not create pipe\n\n";
        }

        pid2 = fork();
        if (pid2 == 0)
        {
            // grandchild process executes here for output side of pipe
            close(1);
            dup2(fd_pipe[1], 1);
            close(fd_pipe[0]);
            close(fd_pipe[1]);
            execvp(args2[0], args2);
            perror("Command execution failed: ");
            exit(5);
        }

        waitpid(pid2, NULL, 0);

        // back to process for input side of pipe
        close(0);
        dup2(fd_pipe[0], 0);
        close(fd_pipe[0]);
        close(fd_pipe[1]);
        execvp(args1[0], args1);
        perror("Command execution failed: ");
        exit(5);
    }
    else
    {
        std::cout << "Child process stated. Pid: " << pid1 << "\n\n";
    }

    pid1 = wait(&stat);

    struct rusage usage;
    getrusage(RUSAGE_CHILDREN, &usage);
    std::cout << "\nStats for child processes";
    std::cout << "\n  User Time:   " << usage.ru_utime.tv_usec << " ms";
    std::cout << "\n  System Time: " << usage.ru_stime.tv_usec << " ms";
    std::cout << "\n  Page Faults: " << usage.ru_majflt;
    std::cout << "\n  Swaps:       " << usage.ru_nswap << "\n\n";
}