#include "tools.h"

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will split a string into a list of stirings by the deliminator
 * 
 * @param[in] input - the string to split
 * @param[in] delim - the string to deliminate by
 * 
 * @return - std::list<std::string> list of strings broken by the delim
 ******************************************************************************/
std::list<std::string> splitString(std::string input, const std::string delim)
{
    std::list<std::string> output;
    size_t sub;

    if(input.empty())
    {
        return output;
    }

    while((sub = input.find(delim)) != std::string::npos)
    {
        output.push_back(input.substr(0, sub));
        input.erase(0, sub + delim.length());
    }

    if(input.length() > 0)
    {
        output.push_back(input);
    }

    return output;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will tell you if string is an int
 * 
 * @param[in] input - the string to check
 * 
 * @return - bool
 ******************************************************************************/
bool isInt(std::string input)
{
    for(char c : input)
    {
        if(!std::isdigit(c))
        {
            return false;
        }
    }

    return true;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will fill a char array with the contents of a list of strings
 * 
 * @param[in] stringList - the list of strings convert
 * @param[in] charArray - the array of chars that will be filled
 ******************************************************************************/
void buildCharArrayFromList(std::list<std::string> stringList, char** charArray)
{
    int pos = 0;

    for(std::string s : stringList)
    {
        charArray[pos] = new char[s.length()];
        strcpy(charArray[pos], s.c_str());
        pos++;
    }

    charArray[pos] = NULL;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will split a string into a list of stirings by the deliminator
 * 
 * @param[in] listOfStrings - the list of strings to split
 * @param[in] delim - the string to deliminate by
 * 
 * @return - list of lists of strings broken by the delim
 ******************************************************************************/
std::list<std::list<std::string>> splitList(std::list<std::string> listOfStrings, const std::string delim)
{
    std::list<std::string>::iterator delimPos;
    std::list<std::list<std::string>> splitList;
    std::list<std::string> subList;

    delimPos = std::find(listOfStrings.begin(), listOfStrings.end(), delim);

    while(delimPos != listOfStrings.end())
    {
        subList.splice(subList.begin(), listOfStrings, listOfStrings.begin(), delimPos);

        splitList.push_back(subList);

        listOfStrings.pop_front();

        delimPos = std::find(listOfStrings.begin(), listOfStrings.end(), delim);
    }

    splitList.push_back(listOfStrings);

    return splitList;
}
