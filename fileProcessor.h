/**************************************************************************//**
 * @file
 * @brief This is the header file for fileProcessor.cpp
 *****************************************************************************/
#ifndef __FILEPROCESSOR_H
#define __FILEPROCESSOR_H

#include <iostream>
#include <fstream>
#include <list>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

/*!
* @brief This will process files in the system
*/
class FileProcessor
{
    public:
        FileProcessor();

        bool isFolder(std::string);
        bool isFile(std::string);
        std::list<std::string> getFileContents(std::string);
        std::string getCurrentPath();
        std::list<std::string> getSubDirectories(std::string);
};

#endif