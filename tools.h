/**************************************************************************//**
 * @file
 * @brief This is the header file for tools.cpp
 *****************************************************************************/
#ifndef __TOOLS_H
#define __TOOLS_H

#include <iostream>
#include <cstring>
#include <string>
#include <list>
#include <algorithm>

std::list<std::string> splitString(std::string, const std::string);
bool isInt(std::string);
void buildCharArrayFromList(std::list<std::string>, char**);
std::list<std::list<std::string>> splitList(std::list<std::string>, const std::string);

#endif
