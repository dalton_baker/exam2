#include <iostream>
#include <string>
#include "inputProcessor.h"

int main (int argc, char* const argv[])
{
    bool askForInput = true;
    std::string input;
    InputProcessor inputProcessor;

    while(askForInput)
    {
        std::cout << "\033[1;32mdash>\033[0m";
        if(!std::getline(std::cin, input))
        {
            std::cout << "\n\n";
            return 0;
        }

        askForInput = input != "exit";
        
        if(askForInput && !input.empty())
        {
            inputProcessor.processInput(input);
        }
    }
    
    return 0;
}